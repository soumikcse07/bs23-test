package com.madroid.bs23test.data.repositories

import android.util.Log
import com.madroid.bs23test.core.utils.Connectivity
import com.madroid.bs23test.core.utils.Constants
import com.madroid.bs23test.core.utils.Resource
import com.madroid.bs23test.data.models.Data
import com.madroid.bs23test.data.source.IBookApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class BookRepoImpl @Inject constructor(private val apiService: IBookApiService,private val connectivity: Connectivity) : IBookRepo {
    companion object {
        private const val TAG = "BookRepoImpl"
    }

    override suspend fun fetchBookList(): Flow<Resource<List<Data>>> = flow {
        try {
            if (connectivity.isConnected()) {
                val response = apiService.fetchBookList()
                if (response.isSuccessful && response.code() == 200 && response.body() != null) {
                    if (response.body()!!.status == 200) {
                        val body = response.body()
                        if (body?.data.isNullOrEmpty()) {
                            emit(Resource.Failed(response.body()!!.message ?: Constants.NO_BOOKS_FOUND))
                        } else {
                            emit(Resource.Success(body?.data!!))
                        }
                    } else {
                        emit(Resource.Failed(response.body()!!.message ?: Constants.ERROR_MSG))
                    }
                } else {
                    emit(Resource.Failed(Constants.ERROR_MSG))
                }
            } else {
                emit(Resource.Failed(Constants.NO_NETWORK_AVAILABLE))
            }

        } catch (e: Exception) {
            Log.e(TAG, "fetchBookList: ${e.message}", e)
            emit(Resource.Failed(e.message ?: Constants.ERROR_MSG))
        }
    }.flowOn(Dispatchers.IO).catch {
        Log.e(TAG, "fetchBookList: ${it.message}", it)
        emit(Resource.Failed(it.message ?: Constants.ERROR_MSG))
    }
}