package com.madroid.bs23test.core.di

import android.util.Log
import com.madroid.bs23test.BuildConfig
import com.madroid.bs23test.core.utils.Constants
import com.madroid.bs23test.data.source.IBookApiService
import com.madroid.bs23test.data.source.mockResponse
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    companion object {
        private const val TAG = "NetworkModule"
    }

    @Singleton
    @Provides
    fun provideRetrofitClient(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .baseUrl(Constants.BASE_URL)
            .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor { chain ->
                val request = chain.request()
                val requestBuilder = request.newBuilder().url(request.url()).build()
                Log.d(TAG, "provideOkHttpClient(): request: $requestBuilder")
                var response = chain.proceed(request)
                Log.d(TAG, "provideOkHttpClient: response code: ${response.code()}")

                if (response.code()!=200 && BuildConfig.IS_DEV) {
                    response = okhttp3.Response.Builder()
                        .protocol(Protocol.HTTP_2)
                        .request(chain.request())
                        .code(200)
                        .message("Success")
                        .body(ResponseBody.create(MediaType.parse("text/json"), mockResponse))
                        .build()
                }

                response
            }
            .build()
    }

    @Singleton
    @Provides
    fun provideGitApiService(retrofit: Retrofit): IBookApiService {
        return retrofit.create(IBookApiService::class.java)
    }

}