package com.madroid.bs23test.core.utils

object Constants {
    const val NO_BOOKS_FOUND: String = "No books found"
    const val BASE_URL = "https://example.com/api/"
    const val ERROR_MSG = "Something went wrong! please try after sometime"
    const val NO_NETWORK_AVAILABLE = "No internet connection available"
}