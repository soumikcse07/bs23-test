package com.madroid.bs23test.ui.feed

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.madroid.bs23test.core.utils.loadImage
import com.madroid.bs23test.data.models.Data
import com.madroid.bs23test.databinding.ItemBooksBinding

class BookListAdapter: ListAdapter<Data,BookListAdapter.BookHolder>(
    object : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.id==newItem.id
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

    }
) {

    private var onBooksClicked: ((Data)->Unit)?=null
    inner class BookHolder(private val binding: ItemBooksBinding): ViewHolder(binding.root) {
        fun bind(data: Data) {
            binding.apply {
                tvBookName.text = data.name
                ivPhoto.loadImage(data.image?:"")

                root.setOnClickListener {
                    onBooksClicked?.invoke(data)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookHolder {
        return BookHolder(ItemBooksBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: BookHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun onBooksClicked(listener: (Data)->Unit) {
        onBooksClicked = listener
    }
}