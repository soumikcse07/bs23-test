package com.madroid.bs23test.ui.details

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavArgument
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.madroid.bs23test.R
import com.madroid.bs23test.core.utils.launchUrl
import com.madroid.bs23test.core.utils.loadImage
import com.madroid.bs23test.databinding.FragmentBookDetailsBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class BookDetailsFragment : Fragment() {
    private var _binding: FragmentBookDetailsBinding?=null
    private val arg: BookDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBookDetailsBinding.inflate(layoutInflater)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
    }

    @SuppressLint("SetTextI18n")
    private fun setViews() {
        _binding?.apply {
            tvName.text = "Name: ${arg.data?.name}"
            tvUrl.text = "Url: ${arg.data?.url}"
            ivFullPhoto.loadImage(arg.data?.image?:"")

            tvUrl.setOnClickListener {
                requireContext().launchUrl(arg.data?.url)
            }

            toolbar.setNavigationOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}