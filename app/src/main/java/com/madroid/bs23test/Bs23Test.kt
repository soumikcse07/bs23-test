package com.madroid.bs23test

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Bs23Test: Application()