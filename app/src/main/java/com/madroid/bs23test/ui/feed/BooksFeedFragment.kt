package com.madroid.bs23test.ui.feed

import androidx.fragment.app.viewModels
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.madroid.bs23test.R
import com.madroid.bs23test.core.utils.showSnackBar
import com.madroid.bs23test.databinding.FragmentBooksFeedBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class BooksFeedFragment : Fragment() {

    companion object {
        private const val TAG = "BooksFeedFragment"
    }

    private var _binding: FragmentBooksFeedBinding? = null
    private val viewModel: BooksFeedViewModel by viewModels()
    private val mBookListAdapter: BookListAdapter by lazy {
        BookListAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBooksFeedBinding.inflate(layoutInflater)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
        setObservers()
    }

    private fun setViews() {
        _binding?.apply {
            rvBookList.apply {
                layoutManager = GridLayoutManager(requireContext(), 2)
                adapter = mBookListAdapter
            }
        }

        mBookListAdapter.apply {
            onBooksClicked {
                findNavController().navigate(
                    BooksFeedFragmentDirections.actionBooksFeedFragmentToBookDetailsFragment(
                        it
                    )
                )
            }
        }
    }

    private fun setObservers() {
        viewModel.apply {
            lifecycleScope.launch {
                uiState.collectLatest {
                    when (it) {
                        is UiState.Failed -> {
                            Log.d(TAG, "setObservers: failed: ${it.message}")
                            _binding?.apply {
                                progressBar.visibility = View.GONE
                            }
                            showSnackBar(_binding, it.message)
                        }

                        UiState.Initial -> fetchBooks()
                        UiState.Loading -> {
                            _binding?.apply {
                                progressBar.visibility = View.VISIBLE
                                rvBookList.visibility = View.GONE
                            }
                        }

                        is UiState.Success -> {
                            _binding?.apply {
                                progressBar.visibility = View.GONE
                                rvBookList.visibility = View.VISIBLE
                            }
                            mBookListAdapter.submitList(it.books)
                        }
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}