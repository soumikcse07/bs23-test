package com.madroid.bs23test.core.di

import com.madroid.bs23test.data.repositories.BookRepoImpl
import com.madroid.bs23test.data.repositories.IBookRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Singleton
    @Binds
    abstract fun bindBookRepo(repo: BookRepoImpl): IBookRepo
}