package com.madroid.bs23test.data.repositories

import com.madroid.bs23test.core.utils.Resource
import com.madroid.bs23test.data.models.Data
import kotlinx.coroutines.flow.Flow

interface IBookRepo {
    suspend fun fetchBookList() : Flow<Resource<List<Data>>>
}