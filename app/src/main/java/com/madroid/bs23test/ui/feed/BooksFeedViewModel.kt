package com.madroid.bs23test.ui.feed

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.madroid.bs23test.core.utils.Constants
import com.madroid.bs23test.data.models.Data
import com.madroid.bs23test.data.repositories.IBookRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BooksFeedViewModel @Inject constructor(
    private val repo: IBookRepo
) : ViewModel() {
    companion object {
        private const val TAG = "BooksFeedViewModel"
    }

    private val _uiState = MutableStateFlow<UiState>(UiState.Initial)
    val uiState: StateFlow<UiState> get() = _uiState

    fun fetchBooks() {
        viewModelScope.launch {
            _uiState.value = UiState.Loading
            repo.fetchBookList().catch {
                Log.e(TAG, "fetchBooks: ${it.message}", it)
                _uiState.value = UiState.Failed(it.message?:Constants.ERROR_MSG)
                }.collect {
                    if (it.data!=null) {
                        _uiState.value = UiState.Success(it.data)
                    } else if(it.message!=null) {
                        _uiState.value = UiState.Failed(it.message)
                    }
                }
        }
    }
}

sealed class UiState {
    data object Initial : UiState()
    data object Loading : UiState()
    class Success(var books: List<Data>) : UiState()
    class Failed(var message: String) : UiState()
}