package com.madroid.bs23test.core.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar

fun ImageView.loadImage(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

fun Fragment.showSnackBar(
    binding: ViewBinding?,
    message: String,
    length: Int = Snackbar.LENGTH_SHORT
) {
    if (isAdded && binding!=null)
        Snackbar.make(binding.root, message, length).show()
}

fun Context.launchUrl(url: String?) {
    try {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    } catch (e: Exception) {
        e.printStackTrace()
        Toast.makeText(this, Constants.ERROR_MSG, Toast.LENGTH_SHORT).show()
    }
}